﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoidContainer
{
    public class ContDataModel
    {
        public string CustPart;
        public string Product;
        public string ContStatus;
        public int Qty;
        public int UID;
        public string Location;
    }
}
