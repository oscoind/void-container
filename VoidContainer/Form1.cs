﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VoidContainer
{
    public partial class Form1 : Form
    {
        public string CompanyID;

        public Form1()
        {
            InitializeComponent();

            string[] args = Environment.GetCommandLineArgs();                  // get command line parameters
            CompanyID = args[1];                                               // assign parameter to a variable

            switch (CompanyID)                                                 // set label text for company ID
            {
                case "JACK":
                    label2.Text = "Jackson Division";
                    break;
                case "NEWB":
                    label2.Text = "New Boston Division";
                    break;
                case "PORT":
                    label2.Text = "Portsmouth Division";
                    break;
                case "TPO":
                    label2.Text = "TEST Portsmouth Division";
                    break;
                default:
                    MessageBox.Show("Invalid company ID - " + CompanyID + " - program will terminate");
                    //Application.Exit();
                    this.Close();
                    break;
            }
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)                                       // Process "add" button when ENTER key pressed on form
            {
                buttonAdd_Click(sender, e);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listView1.View = View.Details;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text)) return;                   // return when no entry is made for container ID 

            ContDataModel ContData = new ContDataModel();
            ContData = GetContData(textBox1.Text);

            if (string.IsNullOrEmpty(ContData.ContStatus)) return;             // return when no data is retrieved for container ID 

            if (ContData.ContStatus != "OUT")                                  // wrong status for container
            {
                MessageBox.Show("Invalid container status (not OUTSIDE).  Enter another container ID.");
                textBox1.Clear();
                return;
            }

            ListViewItem FoundItem = listView1.FindItemWithText(textBox1.Text);  // search for container - error if found 
            if (FoundItem != null)
            {
                MessageBox.Show("Container ID already in list.  Enter another container ID.");
                textBox1.Clear();
                return;
            }

            ListViewItem row = new ListViewItem(textBox1.Text);
            row.SubItems.Add(ContData.CustPart);
            row.SubItems.Add(ContData.Product);
            row.SubItems.Add(ContData.Qty.ToString());
            row.SubItems.Add(ContData.UID.ToString());
            row.SubItems.Add(ContData.Location);
            listView1.Items.Add(row);
            textBox1.Clear();
            textBox1.Focus();
        }

        private ContDataModel GetContData(string ContID)
        {
            ContDataModel RetContData = new ContDataModel();

            using (OODService.OODSoapClient client = new OODService.OODSoapClient())
            {
                string ErrMsg = string.Empty;

                string sql = "select Custpart, Product, Qty, ContStatus, UniqueID, Location from ContainerInv where companyID = '{0}' and ContainerID = {1}";

                DataSet ds = client.FetchDataSQL(CompanyID, "ooduser", "qlqpwlac", string.Format(sql, CompanyID, ContID), 0, 0, out ErrMsg);

                if (!ds.IsInitialized || ds.Tables.Count < 1)                  // error in query or could not connect
                {
                    MessageBox.Show("Error connecting to Odyssey database.  Try again later.");
                    textBox1.Clear();
                }
                else if (ds.Tables[0].Rows.Count < 1)                          // did not find container ID
                {
                    MessageBox.Show("Could not find container ID.  Enter another container ID.");
                    textBox1.Clear();
                }
                else                                                           // sql query for container successful
                {
                    var row = ds.Tables[0].Select().FirstOrDefault();
                    RetContData.CustPart = row.Field<string>("Custpart");
                    RetContData.Product = row.Field<string>("Product");
                    RetContData.ContStatus = row.Field<string>("ContStatus");
                    RetContData.Qty = row.Field<int>("Qty");
                    RetContData.UID = row.Field<int>("UniqueID");
                    RetContData.Location = row.Field<string>("Location");
                }
            }

            return RetContData;
        }

        private void buttonRemoveALL_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0) return;                    // return when no entry is selected to be removed
            listView1.SelectedItems[0].Remove();
        }

        private void buttonProcess_Click(object sender, EventArgs e)
        {
            if (listView1.Items.Count == 0) return;                            // return when no entries exist to be processed

            bool bSuccess = false;
            string ErrFld = string.Empty;
            string FieldList = string.Empty;
            string ValueList = string.Empty;

            //----- 
            // process the container IDs in the LISTVIEW object - update fields in file containerinv
            //-----

            foreach (ListViewItem item in listView1.Items)
            {
                // MessageBox.Show("Item is - " + item.Text + "   " + item.SubItems[1].Text + "   " + item.SubItems[2].Text + "   " + item.SubItems[3].Text + "   " + item.SubItems[4].Text + "   " + item.SubItems[5].Text);

                FieldList = "ContStatus|PickListItem|PickListNbr";
                ValueList = "AVL|0|0";

                using (OODService.OODSoapClient client = new OODService.OODSoapClient())
                {
                    string ErrMsg = string.Empty;
                    int UniqueID = Int32.Parse(item.SubItems[4].Text);

                    ErrFld = client.UpdateRecord(CompanyID, "ooduser", "qlqpwlac", "Containerinv", UniqueID, FieldList, ValueList, out bSuccess, out ErrMsg);

                    if (!string.IsNullOrEmpty(ErrMsg))                         // error in update or could not connect
                    {
                        MessageBox.Show("Error updating database - " + ErrMsg + " UID= " + item.SubItems[4].Text);
                        return;
                    }
                    else if (!string.IsNullOrEmpty(ErrFld))                    // did not find container ID
                    {
                        MessageBox.Show("Error occurred updating container ID -" + item.Text);
                        return;
                    }
                    else if (bSuccess == true)                                 // container update successful, continue
                    {

                        //-----
                        // process the container ID in the LISTVIEW object - write 'DB' trans record
                        //-----

                        bSuccess = false;
                        ErrFld = string.Empty;
                        FieldList = string.Empty;
                        ValueList = string.Empty;
                        Int32 NewUID = 0;

                        FieldList = "ContainerID|InvLocation|Qty|Rtgseq|ScreenID|Transcode|AutoPost";
                        ValueList = string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}",
                                    item.Text,
                                    item.SubItems[5].Text,
                                    item.SubItems[3].Text,
                                    100,
                                    "VOID CONTAINER",
                                    "DB",
                                    "yes");

                        {
                            ErrMsg = string.Empty;

                            NewUID = client.AddRecord(CompanyID, "ooduser", "qlqpwlac", "ProductionTran", FieldList, ValueList, out ErrFld, out bSuccess, out ErrMsg);

                            if (!string.IsNullOrEmpty(ErrMsg))                 // error in record add or could not connect
                            {
                                MessageBox.Show("Error writing to database - " + ErrMsg);
                                return;
                            }
                            else if (!string.IsNullOrEmpty(ErrFld))            // error in record add
                            {
                                MessageBox.Show("Error occurred writing field - " + ErrFld);
                                return;
                            }
                            else if (bSuccess == true)                         // write to file was successful, remove item
                            {
                                item.Remove();
                            }
                        }

                    }
                }
            }
        }
    }
}
